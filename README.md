# Git & Fork me !

#### What you need to do?

Your task:

    - Fork this repository
    - Make update (read below what to update)
    - Create your own branch named -> yourName_yourSurname
    - Send merge request using your branch above as source branch until tomorrow 09:00

#### What you should add?

You should briefly explain why we need forking and sending merge requests.

    - Explain purpose of forking
    - Explain the purpose of merge requests
    - Explain the target and source branches in merge requests
    - Go ahead and do labs in lab.github.com regarding to merge(pull) requests

Links:

    - https://lab.github.com/githubtraining/managing-merge-conflicts
    - https://lab.github.com/githubtraining/reviewing-pull-requests

There are a lot of other labs there to strengthen your git knowledge.

***NOTE:*** No need to write essay here, just few words how you undrestood the process. That's it.

--- 

# About Fork

Fork is a copy of repo that can be edited not affecting original repo.


# About Merge requests

Merge(pull) request send  request to check your code if everything ok it will be approved, or declined if there is some errors by TeamLead or other team members.Also it allows to collaborate with team members and write down messages or descriptions for your code.
Also source branch is where updates come and a target branch is the main branch where all approved files are stored.








